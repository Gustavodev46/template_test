import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-avaliacao',
  templateUrl: './avaliacao.component.html',
  styleUrls: ['./avaliacao.component.css']
})
export class AvaliacaoComponent implements OnInit {

  imgCss = ''
  numberAvaliation!:number
  pesquisas: any[] = [
    {
      id: 1,
      pergunta:"Encontrou todos os produtos que precisa?",
      tipo: "satisfação" ,
      ativo: true,
      avaliacao: null
    },
    {
      id: 2,
      pergunta:"Nossa variedade de produtos atendeu sua necessidade?",
      tipo: "satisfação" ,
      ativo: true,
      avaliacao: null
    },
    {
      id: 3,
      pergunta:"Em relação aos preços, como nos avalia?",
      tipo: "satisfação" ,
      ativo: true,
      avaliacao: null
    },
    {
      id: 4,
      pergunta:"O vendendor foi atencioso e prestativo?",
      tipo: "satisfação" ,
      ativo: true,
      avaliacao: null
    },
    {
      id: 5,
      pergunta:"O atendimento no caixa foi satisfatório?",
      tipo: "satisfação" ,
      ativo: true,
      avaliacao: null
    },
    {
      id: 6,
      pergunta:"Em relação a limpeza e organização como nos avalia?",
      tipo: "satisfação" ,
      ativo: true,
      avaliacao: null
    },
    {
      id: 7,
      pergunta:"Você indicaria a Santa Marta pra algum amigo ou familiar?",
      tipo: "pontuação" ,
      ativo: true,
      avaliacao: null
    },
]

  constructor( public router: Router) { }

  ngOnInit() {
  }

  
  agradecimento(){
    // let avaliado = true
    // let falta = 'Faltou avaliar a(s) '
    // this.pesquisas.forEach(element => {
    //    if(!element.avaliacao){
    //      avaliado = false
    //      falta +=`${element.id} º `
    //    } 
    // }
    // );
    // falta += 'pergunta(s)'
    // if(!avaliado) alert(falta)
    // else{
    //   this.router.navigate(['agradecimento'])
    // }
    this.router.navigate(['agradecimento'])
  }

  setPontuacao(){
  }

  imgClick(pesquisa: any){
    pesquisa.avaliacao = 1
    this.imgCss = ''
  }

}
