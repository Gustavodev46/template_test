import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AvaliacaoComponent } from './pesquisa/avaliacao/avaliacao.component';
import { PesquisaModule } from './pesquisa/pesquisa.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PesquisaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
